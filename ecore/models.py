from django.contrib.auth.models import User
from django.db import models

from django.core.validators import RegexValidator


# Create your models here.
class Role(models.Model):
    ROLE_TYPE_CHOICES = (
        ('Admin', 'Admin'),
        ('User', 'User')
    )
    role_type = models.CharField(
        choices=ROLE_TYPE_CHOICES,
        max_length=100,
    )
    first_name = models.CharField(
        max_length=100,
    )
    last_name = models.CharField(
        max_length=100,
    )
    username = models.CharField(
        max_length=100,
    )
    password = models.CharField(
        max_length=100,
    )
    email_id = models.EmailField(
        max_length=100,
    )
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    mobile_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)  # validators should be a list

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.username


class User(models.Model):
    ROLE_TYPE_CHOICES = (
        ('Admin', 'Admin'),
        ('User', 'User')
    )
    STATUS_CHOICES = (
        ('Hold', 'Hold'),
        ('In Progress', 'In Progress'),
        ('Completed', 'Completed'),
    )

    employee_name = models.ForeignKey(
        Role,
        on_delete=models.CASCADE,
    )
    Description = models.TextField(
        null=True,
        blank=True
    )
    Status = models.CharField(
        choices=STATUS_CHOICES,
        max_length=100,
    )

    can_add = models.BooleanField(
        default=False,
    )
    can_edit = models.BooleanField(
        default=False,
    )
    can_view = models.BooleanField(
        default=False,
    )
    can_delete = models.BooleanField(
        default=False
    )
    email_id = models.EmailField(
        max_length=100,
    )
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    mobile_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)  # validators should be a list

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.role_type


class Website(models.Model):
    websitename = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.websitename


# Task Data
class Tasks(models.Model):
    STATUS_CHOICES = (
        ('Hold', 'Hold'),
        ('In Progress', 'In Progress'),
        ('Completed', 'Completed'),
    )
    website = models.ManyToManyField(Website

                                     )
    reported_by = models.CharField(
        max_length = 100,
        null = True,
        blank = True
    )
    description = models.TextField(
        null=True,
        blank=True
    )

    reporting_date = models.DateField()
    expected_closure_date = models.DateField()
    task_type = models.CharField(
        max_length=100,
        blank=True,
        null=True
    )
    assigned_to = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )
    status = models.CharField(
        choices=STATUS_CHOICES,
        max_length=100,
    )

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.task_type


# class Marchent(models.Model):
#     marchentName=models.CharField(max_length=50)
#     code=models.CharField(max_length=4)
#
#     def __str__(self):
#         return self.marchentName
#
# class Lenin(models.Model):
#     marchent = models.ForeignKey(Marchent, on_delete=models.CASCADE)
#     leninName=models.CharField(max_length=50)
#     leninCode=models.CharField(max_length=4)
#
#     def __str__(self):
#         return f"{self.marchent}-{self.leninName}-{self.leninCode}"
# class RFID(models.Model):
#     marchent=models.ForeignKey(Marchent, on_delete=models.CASCADE)
#     leninName=models.ForeignKey(Lenin,on_delete=models.CASCADE)
#
#     RFIDNumber=models.IntegerField()
#
#     def __str__(self):
#         return f"{self.marchent}-{self.leninName} - {self.RFIDNumber}"


