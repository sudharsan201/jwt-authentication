from django.urls import path

from ecore import views

urlpatterns = [

    path('rolelist/', views.Rolelist.as_view()),
    path('roleview/', views.Role_list_view),
    path("roleEdit/<int:id>", views.RoleEdit),
    path('RoleClass/<int:pk>', views.RoleClass.as_view()),

    path('userlist/', views.Userlist.as_view()),
    path('userview/', views.User_view),
    path("userEdit/<int:id>", views.UserEdit),
    path('userclass/<int:pk>', views.UserClass),


    path('websitelist/', views.Websitelist.as_view()),
    path('Website_view/', views.Website_view),
    path("websiteEdit/<int:id>", views.WebsiteEdit),
    path('websiteClass/<int:pk>', views.WebsiteClass),

    path('Tasklist/', views.Tasklist.as_view()),
    path('taskview/', views.Task_view),
    path("taskEdit/<int:id>", views.TaskEdit),
    path('taskClass/<int:pk>', views.TaskClass),


    # path('Marchent/', views.MarchentClass.as_view()),
    # path('marchent1/', views.marchent_view),
    # path("marchent1/<int:id>",views.marchentEdit),
    # path('marchent/<int:pk>', views.MarchentClass1.as_view()),
    #
    #
    #
    # path('Lenin/', views.LeninClass.as_view()),
    # path('lenin1/', views.lenin_view),
    # path("lenin1/<int:id>", views.LeninEdit),
    #
    # path('Rfid/', views.RFIDClass.as_view()),
    # path('rfid1/', views.Rfid_view),
    # path('rfid1/<int:id>', views.RfidEdit),
]
