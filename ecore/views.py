from django.shortcuts import render
from rest_framework import generics, status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from ecore.models import Role, User, Tasks,Website
from ecore.serializers import RoleSerializer, UserSerializer, TaskSerializer,WebsiteSerializer


# Create your views here.
from django.urls import path, include
from ecore.views import *
from ecore import views
from rest_framework import serializers

class Rolelist(generics.ListCreateAPIView):
    def get_queryset(self):
        return Role.objects.all()
    serializer_class = RoleSerializer


class Userlist(generics.ListCreateAPIView):
    def get_queryset(self):
        return User.objects.all()
    serializer_class = UserSerializer


class Websitelist(generics.ListCreateAPIView):
    def get_queryset(self):
        return Website.objects.all()
    serializer_class = WebsiteSerializer


class Tasklist(generics.ListCreateAPIView):
    def get_queryset(self):
        return Tasks.objects.all()
    serializer_class = TaskSerializer


class RoleClass(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        blogs = Role.objects.all()
        return blogs
    serializer_class = RoleSerializer


class UserClass(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        blogs = User.objects.all()
        return blogs
    serializer_class = UserSerializer


class TaskClass(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        blogs = Tasks.objects.all()
        return blogs
    serializer_class = TaskSerializer


class WebsiteClass(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        blogs = Website.objects.all()
        return blogs
    serializer_class = WebsiteSerializer


@api_view(['GET', 'POST'])
def Role_list_view(request):
    if request.method == 'GET':
        blogs = Role.objects.all()
        serializer = RoleSerializer(bolgs, many=True)
        return Response({
            'status': True,
            'message': 'Get request fulfilled!!',
            'data': serializer.data
        })
    if request.method == 'POST':
        serializer = RoleSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({
                'status': True,
                'message': 'Post request fulfilled!!',
                'data': serializer.data
            })
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    return Response({
        'status': False,
        'message': 'Invalid request',
        'data': ''
    })


@api_view(['GET', 'DELETE', 'PUT'])
def RoleEdit(request, id):
    try:
        todo = Role.objects.get(id=id)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = UserSerializer(todo)
        return Response({
            'status': True,
            'message': 'Edit request fulfilled!!',
            'data': serializer.data
        })

    elif request.method == 'PUT':  # update
        serializer = RoleSerializer(todo, data=request.data)
        if serializer.is_valid():
            serializer.save()  # update Table InDB
            return Response({
                'staus': True,
                'message': 'Update request fulfilled!!',
                'data': serializer.data

            })
        else:  # Delete
            todo.delete()
            return Response({
                'status': True,
                'message': 'Edit request fulfilled!!',
                'data': 'succesfully deleted'
            })


@api_view(['GET', 'POST'])  # Employee
def User_view(request):
    if request.method == 'GET':
        blogs = User.objects.all()
        serializer = UserSerializer(blogs, many=True)
        return Response({
            'status': True,
            'message': 'Get request fulfilled!! ',
            'data': serializer.data

        })

    if request.method == 'POST':
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({
                'status': True,
                'message': 'Post request fulfilled!!',
                'data': serializer.data
            })
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    return Response({
        'status': False,
        'message': 'Invalid request',
        'data': ''

    })


@api_view(['GET', 'DELETE', 'PUT'])
def UserEdit(request, id):
    try:
        todo = User.objects.get(id=id)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == 'GET':
        serializer = UserSerializer(todo)
        return Response({
            'status': True,
            'message': 'Edit request fulfilled!!',
            'data': serializer.data
        })

    elif request.method == 'PUT':  # upadate
        serializer = UserSerializer(todo, data=request.data)
        if serializer.is_valid():
            serializer.save()  # update table in DB
            return Response({
                'staus': True,
                'message': 'Update request fulfilled!!',
                'data': serializer.data
            })
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    else:
        todo.delete()
        return Response({
            'status': True,
            'message': 'Edit request fulfilled!!',
            'data': 'succesfully deleted'

        })


@api_view(['GET', 'POST'])
def Website_view(request):
    if request.method == 'GET':
        blogs = Website.objects.all()
        serializer = WebsiteSerializer(blogs, many=True)
        return Response({
            'status': True,
            'message': 'Get request fulfilled!! ',
            'data': serializer.data
        })

    if request.method == 'POST':

        serializer = WebsiteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({
                'status': True,
                'message': 'Post request fulfilled!!',
                'data': serializer.data
            })
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    return Response({
        'status': False,
        'message': 'Invalid request',
        'data': ''
    })


@api_view(['GET', 'DELETE', 'PUT'])
def WebsiteEdit(request, id):
    try:
        todo = Website.objects.get(id=id)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = WebsiteSerializer(todo)
        return Response({
            'status': True,
            'message': 'Edit request fulfilled!!',
            'data': serializer.data
        })

    elif request.method == 'PUT':  # Update
        serializer = WebsiteSerializer(todo, data=request.data)
        if serializer.is_valid():
            serializer.save()  # Update table in DB
            return Response({
                'staus': True,
                'message': 'Update request fulfilled!!',
                'data': serializer.data
            })
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)  # Bad request


    else:  # Delete
        todo.delete()
        return Response({
            'status': True,
            'message': 'Edit request fulfilled!!',
            'data': 'succesfully deleted'
        })


@api_view(['GET', 'POST'])
def Task_view(request):
    if request.method == 'GET':
        blogs = Tasks.objects.all()
        serializer = TaskSerializer(blogs, many=True)
        return Response({
            'status': True,
            'message': 'Get request fulfilled!! ',
            'data': serializer.data
        })

    if request.method == 'POST':

        serializer = TaskSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({
                'status': True,
                'message': 'Post request fulfilled!!',
                'data': serializer.data
            })
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    return Response({
        'status': False,
        'message': 'Invalid request',
        'data': ''
    })


@api_view(['GET', 'DELETE', 'PUT'])
def TaskEdit(request, id):
    try:
        todo = Tasks.objects.get(id=id)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = WebsiteSerializer(todo)
        return Response({
            'status': True,
            'message': 'Edit request fulfilled!!',
            'data': serializer.data
        })

    elif request.method == 'PUT':  # Update
        serializer = TaskSerializer(todo, data=request.data)
        if serializer.is_valid():
            serializer.save()  # Update table in DB
            return Response({
                'staus': True,
                'message': 'Update request fulfilled!!',
                'data': serializer.data
            })
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)  # Bad request


    else:  # Delete
        todo.delete()
        return Response({
            'status': True,
            'message': 'Edit request fulfilled!!',
            'data': 'succesfully deleted'
        })



# class MarchentClass(generics.ListCreateAPIView):
#     def get_queryset(self):
#         return Marchent.objects.all()
#
#     serializer_class = MarchentSerializer
#
# class LeninClass(generics.ListCreateAPIView):
#     def get_queryset(self):
#         return Lenin.objects.all()
#
#     serializer_class = LeninSerializer
# class RFIDClass(generics.ListCreateAPIView):
#     def get_queryset(self):
#         return RFID.objects.all()
#
#     serializer_class = RFIDSerializer
#
# # update Data
#
# class MarchentClass1(generics.RetrieveUpdateDestroyAPIView):
#     def get_queryset(self):
#         blogs=Marchent.objects.all()
#         return blogs
#
#     serializer_class = MarchentSerializer
#
#
#
# @api_view(['GET', 'POST'])  #Marchent
# def marchent_view(request):
#     if request.method == 'GET':
#         blogs = Marchent.objects.all()
#         serializer = MarchentSerializer(blogs, many=True)
#         return Response({
#                 'status': True,
#                 'message': 'Get request fulfilled!! ',
#                 'data': serializer.data
#             })
#
#     if request.method == 'POST':
#
#         serializer = MarchentSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response({
#                 'status': True,
#                 'message': 'Post request fulfilled!!',
#                 'data': serializer.data
#             })
#         else:
#             return  Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
#
#
#     return Response({
#             'status': False,
#             'message': 'Invalid request',
#             'data': ''
#         })
#
# @api_view(['GET', 'DELETE', 'PUT']) #MarchentUpdation
# def marchentEdit(request, id):
#     try:
#         todo = Marchent.objects.get(id=id)
#     except:
#         return Response(status=status.HTTP_404_NOT_FOUND)
#
#     if request.method == 'GET':
#         serializer = MarchentSerializer(todo)
#         return Response({
#                 'status': True,
#                 'message': 'Edit request fulfilled!!',
#                 'data': serializer.data
#             })
#
#     elif request.method == 'PUT':  # Update
#         serializer = MarchentSerializer(todo, data=request.data)
#         if serializer.is_valid():
#             serializer.save()  # Update table in DB
#             return Response({
#                 'staus': True,
#                 'message': 'Update request fulfilled!!',
#                 'data': serializer.data
#             })
#         else:
#             return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)  # Bad request
#
#
#     else:  # Delete
#         todo.delete()
#         return Response({
#                 'status': True,
#                 'message': 'Edit request fulfilled!!',
#                 'data': 'succesfully deleted'
#             })
#
#
#
#
# @api_view(['GET', 'POST'])  #Lenin
# def lenin_view(request):
#     if request.method == 'GET':
#         blogs = Lenin.objects.all()
#         serializer = LeninSerializer(blogs, many=True)
#         return Response({
#                 'status': True,
#                 'message': 'Get request fulfilled!! ',
#                 'data': serializer.data
#             })
#
#     if request.method == 'POST':
#
#         serializer = LeninSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response({
#                 'status': True,
#                 'message': 'Post request fulfilled!!',
#                 'data': serializer.data
#             })
#         else:
#             return  Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
#
#
#     return Response({
#             'status': False,
#             'message': 'Invalid request',
#             'data': ''
#         })
#
#
# @api_view(['GET', 'DELETE', 'PUT']) #LeninUpdation
# def LeninEdit(request, id):
#     try:
#         todo = Lenin.objects.get(id=id)
#     except:
#         return Response(status=status.HTTP_404_NOT_FOUND)
#
#     if request.method == 'GET':
#         serializer = LeninSerializer(todo)
#         return Response({
#                 'status': True,
#                 'message': 'Edit request fulfilled!!',
#                 'data': serializer.data
#             })
#
#     elif request.method == 'PUT':  # Update
#         serializer = LeninSerializer(todo, data=request.data)
#         if serializer.is_valid():
#             serializer.save()  # Update table in DB
#             return Response({
#                 'staus': True,
#                 'message': 'Update request fulfilled!!',
#                 'data': serializer.data
#             })
#         else:
#             return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)  # Bad request
#
#
#     else:  # Delete
#         todo.delete()
#         return Response({
#                 'status': True,
#                 'message': 'Edit request fulfilled!!',
#                 'data': 'succesfully deleted'
#             })
#
#
#
#
# @api_view(['GET', 'POST'])
# def Rfid_view(request):
#     if request.method == 'GET':
#         blogs = RFID.objects.all()
#         serializer = RFIDSerializer(blogs, many=True)
#         return Response({
#                 'status': True,
#                 'message': 'Get request fulfilled!! ',
#                 'data': serializer.data
#             })
#
#     if request.method == 'POST':
#
#         serializer = RFIDSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response({
#                 'status': True,
#                 'message': 'Post request fulfilled!!',
#                 'data': serializer.data
#             })
#         else:
#             return  Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
#
#
#     return Response({
#             'status': False,
#             'message': 'Invalid request',
#             'data': ''
#         })
#
#
# @api_view(['GET', 'DELETE', 'PUT']) #RFIDUpdation
# def RfidEdit(request, id):
#     try:
#         todo = RFID.objects.get(id=id)
#     except:
#         return Response(status=status.HTTP_404_NOT_FOUND)
#
#     if request.method == 'GET':
#         serializer = RFIDSerializer(todo)
#         return Response({
#                 'status': True,
#                 'message': 'Edit request fulfilled!!',
#                 'data': serializer.data
#             })
#
#     elif request.method == 'PUT':  # Update
#         serializer = RFIDSerializer(todo, data=request.data)
#         if serializer.is_valid():
#             serializer.save()  # Update table in DB
#             return Response({
#                 'staus': True,
#                 'message': 'Update request fulfilled!!',
#                 'data': serializer.data
#             })
#         else:
#             return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)  # Bad request
#
#
#     else:  # Delete
#         todo.delete()
#         return Response({
#                 'status': True,
#                 'message': 'Edit request fulfilled!!',
#                 'data': 'succesfully deleted'
#             })
#
