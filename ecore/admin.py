from django.contrib import admin

# Register your models here.
from ecore.models import Role, User, Tasks,Website

admin.site.register(Role)
admin.site.register(User)
admin.site.register(Tasks)
admin.site.register(Website)